# SCASB - Calcul Itinéraires
Carte interractive de calcul d'intinéraire vélos.

https://home.gegeweb.org/parcours/

![Copie écran](screenshot.png)

## Pourquoi ?
L'idée initiale de faire mon propre éditeur de parcours pour le club FFVélo [SCASB](https://www.scasb.org) n'était pas nouvelle. Mais c'était (du moins je l'imaginais) un projet ambitieux…

[Strava](https://www.strava.com) que j'utilise par ailleurs vient de modifier sa politique, et l'édition des parcours n'est plus disponible pour les comptes basiques (non payant), d'ailleurs il ne reste plus grand chose pour les comptes basiques… 

Je n'étais pas satisfait des solution alternatives existante, alors c'était le moment : « *juste fais le !* » (*).

Finalement, le projet tel qu'il est (était) à fin mai 2020, est sorti en une semaine à peine ! ;)

(*) *Évidement, ça n'est pas donné à tous le monde, il faut quelques compétences en programmation « WEB » (ça tombe bien, j'en ai un peu…). C'est pourquoi j'ai donc décidé de partager ce travail afin qu'il puisse profiter et être librement utilisé et adapté par d'autres que moi. En éspérant qu'il soit utile !*

## Technologies utilisées
- [Leaflet](https://leafletjs.com/) - A JS library for interractive maps
- [Leaflet Sidebar v2](https://github.com/noerw/leaflet-sidebar-v2) – A responsive sidebar for [Leaflet](https://leafletjs.com/).
- [Leaflet Control Geocoder](https://github.com/perliedman/leaflet-control-geocoder) – A simple geocoder for [Leaflet](https://leafletjs.com/) that by default uses [OSM](https://www.openstreetmap.org/)/[Nominatim](https://wiki.openstreetmap.org/wiki/Nominatim).
- [Leaflet Routing Machine](http://www.liedman.net/leaflet-routing-machine/) – An easy, flexible and extensible way to add routing to a [Leaflet](https://leafletjs.com/) map. (*version modifiée*)
- [Leaflet Routing Machine - OpenRoute Service](https://framagit.org/gegeweb/leaflet-routing-machine-openroute/) (dévelopé pour ce projet)
- [leaflet-elevation.js](https://github.com/Raruto/leaflet-elevation) – A [Leaflet](https://leafletjs.com/) plugin that allows to add elevation profiles using [D3.js](https://d3js.org/)
- [D3.js](https://d3js.org/) – Data-Driven Documents (*utilisé par [Leaflet Elevation](https://github.com/Raruto/leaflet-elevation)*)
- [togpx](https://github.com/tyrasd/togpx) – Converts GeoJSON to GPX (*Export de la trace*)
- [Turf.js](https://turfjs.org/) – Advanced geospatial analysis for browsers and Node.js
- [ToGeoJSON](https://github.com/tmcw/togeojson) – Convert KML and GPX to GeoJSON
- [shortcode.js](http://www.salesianer.de/util/osmshortlinks.php) – OSM ShortLinks

## Calcul itinéraires et Géocodage
- [Openroute Service](https://openrouteservice.org/)

## Cartes
- [OpenStreetMap France](http://www.openstreetmap.fr/)
- [Cyclo OSM](https://www.cyclosm.org)
- [OpenStreetMap Mapnik](https://www.openstreetmap.org)
- [Cartes IGN](https://www.geoportail.gouv.fr/)
- [Hillshading NASA SRTM](https://www.nasa.gov/)

## TODO
- [x] Ajout des profils altimétriques,
- [x] Export de la route au format GPX,
- [x] Export de la trace au format GPX,
- [x] Modification des options d'itinéraires (sélection du type de vélo),
- [ ] Paramétrage du comportement des fonctions de localisation coté utilisateur,
- [x] Affichage/calcul des temps de parcours en fonction de ~~la vitesse moyenne de l'utilisateur~~ du type de vélo. (Ajout d'une option dans le plugin ORS pour spécifier la vitesse moyenne estimée en fonction du type de vélo choisi),
- [ ] ~~Stockage des traces sur le serveur~~ (* Abandonné)

(*) *Keep it simple and stupid!* : finalement pour une version auto-hébergeable sur n'importe quel hébergement mutualisé avec ou sans langage coté serveur, implémenter le stockage des traces sur le serveur serait une mauvaise idée. Et d'autre part ça implique la gestion des utilisateurs, ce qui est un autre projet bien plus vaste !

## Utilisation / Installation
1. Clonez ou téléchargez ce dépôt (toutes les librairies tierces sont incluses).
2. Obtenez une clef de l'API [Openroute Service](https://openrouteservice.org/dev/#/signup)
3. Créer ou éditer le fichier `htdocs/config/ors_token.txt` avec votre clef de l'API Openroute Service.
4. Téléchargez le contenu du répertoire `htdocs/` dans le répertoire de votre choix sur votre serveur.
5. Rendez vous à votre URL et profitez !

## Limitations
Du fait de l'utilisation de l'API Openroute sur les serveurs Openroute, la distance maximale d'un itinéraire vélo est de 300km environ. (cf. https://openrouteservice.org/restrictions/)

## Remerciements
Un merci spécial à [@sp3r4z](https://framagit.org/Sp3r4z), sans qui cette idée n'aurait pas germée aussi vite ! ;)

Un grand merci à l'équipe derrière [Openroute Service](https://openrouteservice.org/) pour la fourniture du service,son développement et la libre utilisation de l'API.

Merci à l'ensemble des auteurs des plugins et librairies utilisées.

Et enfin, merci à l'ensemble des contributeurs [OpenStreetMap](https://www.openstreetmap.org/copyright).

---

*N'hésitez pas à me laisser un message sur [Mastodon](https://stoneartprod.xyz/@gegeweb) si vous utilisez et appréciez cette carte.*

*Et n'hésitez pas à créer un ticket si vous constatez un bug ou avez une idée d'amélioration.*
